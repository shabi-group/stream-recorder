import os
import time
import numpy as np
import cv2
import yaml
import random
import math


def crop_border(img, percent=0.15):
    """
    Function to crop image borders by some percent of height and width
    :param img: frame
    :param percent: percent to crop
    :return: cropped image
    """
    # Crop image borders by some percent of height and width
    h, w, c = img.shape
    hcrp, wcrp = int(percent * h), int(percent * w)
    return img[hcrp:-hcrp, wcrp:-wcrp, :]


def initialize_stream(src, attempts, wait):
    """
    Initializing stream from source
    :param src: udp source
    :param attempts: int number for number of initializing attempts
    :param wait: waiting time between attempts
    :return: stream
    """
    tries = 0
    try:
        stream = cv2.VideoCapture(src)
    except:
        num_dots = tries % 4
        print("\rOpenCV Streaming failed, reinitializing" + '.' * num_dots, end='')
        tries += 1
        time.sleep(wait)
        if tries == attempts:
            print("Error catching stream")
            return None
    return stream


def color_test(img):
    """
    Function to check if the image is colored only
    :param img: frame
    :return: True - color only | False - else
    """
    # Crop image and evenly spaced grid across it
    sample = crop_border(img).astype(np.int8)[::10, ::10, :]
    diff1 = np.mean(np.abs(sample[:, :, 0] - sample[:, :, 1]))  # mean |b - g|
    diff2 = np.mean(np.abs(sample[:, :, 1] - sample[:, :, 2]))  # mean |g - r|
    diff3 = np.mean(np.abs(sample[:, :, 2] - sample[:, :, 0]))  # mean |r - b|
    # Take Maximum of those and compare to 3.
    return np.max([diff1, diff2, diff3]) > 3


def generate_name(name, num):
    """
    Generate name for the outgoing video
    :param name: name to give
    :param num: number of the catted video
    :return: output video name
    """
    path = name + "-{}.mp4".format(num)
    return path


def is_exist(name, videos):
    """
    Check if the given video already been made
    :param name: video to check
    :param videos: list to check in
    :return: True - already exists | False - never existed
    """
    for video in videos:
        if name == video:
            return True
    return False


def channel_generate(channels, curr=None):
    """
    Generate a new stream channel number
    :param channels: list of optional channels
    :param curr: current channel to be excluded
    :return: new channel number
    """
    if curr is None:
        return random.choice(list(channels.keys()))
    else:
        return random.choice([i for i in list(channels.keys()) if i not in [curr]])


def cut_video():
    """
    Main function for cutting the stream into small videos
    :return: list of videos made by the function
    """
    try:
        vids = []
        cfg = yaml.load(open(r'Recorder_Config.yaml'), Loader=yaml.Loader)

        # ########## Stream Config ##########
        src = channel_generate(cfg['Channels'])
        attempts_wait = cfg["Waiting_Time_Between_Attempts"]
        max_attempts = cfg["Max_Attempts"]
        stream = initialize_stream(cfg["Channels"][src], attempts=max_attempts,
                                   wait=attempts_wait)
        # ########## Stream Config ##########

        # ########## Writer Config ##########
        video_codec = cv2.VideoWriter_fourcc(*'mp4v')
        width = stream.get(cv2.CAP_PROP_FRAME_WIDTH)
        height = stream.get(cv2.CAP_PROP_FRAME_HEIGHT)
        fps = cfg["Fps"]
        length = cfg["Video_Length"]
        channel_length = cfg["Time_in_channel"]
        vid_number = 1
        output_name = generate_name(cfg["Video_Name"], vid_number)
        writer = cv2.VideoWriter(output_name, video_codec, fps, (int(width), int(height)))
        # ########## Writer Config ##########

        # ########## Cutting Parameters ##########
        video_length_in_frames = int(length * fps * 60)
        channel_length_in_frames = int(channel_length * fps * 60)
        run_length_in_frames = int(cfg["Total_Time"] * fps * 60)
        videos_number = math.ceil(run_length_in_frames/video_length_in_frames)
        color_count = 0
        count = 0
        # ########## Cutting Parameters ##########

        while count < run_length_in_frames:
            try:
                ret, frame = stream.read()
                if ret is True:
                    print("\rNow working on channel - {}, video - {}/{}, frame number - {}/{}"
                          .format(cfg["Channels"][src], vid_number, videos_number, count, run_length_in_frames), end="")
                    if color_test(frame) or cfg["Only_Color"] is False:
                        color_count += 1
                        count += 1
                        writer.write(frame)
                    if (color_count % channel_length_in_frames) == 0 and color_count != 0:
                        stream.release()
                        src = channel_generate(cfg['Channels'], curr=src)
                        stream = initialize_stream(cfg["Channels"][src], attempts=max_attempts,
                                                   wait=attempts_wait)
                    if (color_count % video_length_in_frames) == 0 and color_count != 0:
                        writer.release()
                        print("\nEnded Video number {}".format(vid_number))
                        vid_number += 1
                        color_file_write = open("color_stats.txt", 'a+')
                        color_file_write.write('{}:{}\n'.format(output_name, str(color_count)))
                        color_file_write.close()
                        color_count = 0
                        vids.append(os.path.join(os.curdir, output_name))
                        if count + 1 < run_length_in_frames:
                            output_name = generate_name(cfg["Video_Name"], vid_number)
                            writer = cv2.VideoWriter(output_name, video_codec, fps, (int(width), int(height)))

            except KeyboardInterrupt:
                print("You Chose to finish recording!")
                if is_exist(os.path.join(os.curdir, output_name), vids) is False:
                    writer.release()
                    vids.append(os.path.join(os.curdir, output_name))
                    print("\nEnded Video number {}".format(vid_number))
                    color_file_write = open("color_stats.txt", 'a+')
                    color_file_write.write('{}:{}\n'.format(output_name, str(color_count)))
                    color_file_write.close()
                return vids
            except:
                print("Unexpected Error happened, Goodbye...")
                if is_exist(os.path.join(os.curdir, output_name), vids) is False:
                    writer.release()
                    vids.append(os.path.join(os.curdir, output_name))
                    print("\nEnded Video number {}".format(vid_number))
                    color_file_write = open("color_stats.txt", 'a+')
                    color_file_write.write('{}:{}\n'.format(output_name, str(color_count)))
                    color_file_write.close()
                return vids

        if is_exist(os.path.join(os.curdir, output_name), vids) is False:
            writer.release()
            vids.append(os.path.join(os.curdir, output_name))
            print("\nEnded Video number {}".format(vid_number))
            color_file_write = open("color_stats.txt", 'a+')
            color_file_write.write('{}:{}\n'.format(output_name, str(color_count)))
            color_file_write.close()
        return vids

    except Exception as e:
        print("Error initializing....\n {}".format(e))
        return vids


if __name__ == "__main__":
    vids = cut_video()
    print("#####################################")
    print("########## Your New Videos ##########")
    print("#####################################")
    for vid in vids:
        print("- {}".format(vid))
    print("#####################################")
    print("\nDone.")
